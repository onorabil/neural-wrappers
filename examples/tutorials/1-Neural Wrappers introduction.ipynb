{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Neural Wrappers introduction tutorial\n",
    "\n",
    "This tutorial is a \"how to\" or \"best practice\" around how most things are implemented or expected to be done using the neural wrappers library. The purpose of the library is to simplify training, testing and evaluation steps using the PyTorch library. It implements various helpful functions, such as `train_generator` (similar to a `fit_generator` Keras function), callbacks (`SaveModel`, `PlotMetrics` or custom ones by subclassing `Callback`).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### 1. Model\n",
    "\n",
    "To load the library, all you have to do is subclass `NeuralNetworkPyTorch` instead of PyTorch's `nn.Module`. Basically the class is a subclass of `nn.Module` that has additional methods/fields inserted.\n",
    "\n",
    "`\n",
    "import torch.nn as nn\n",
    "class MyAwesomeNN(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(MyAwesomeNN, self).__init__()\n",
    "        ...\n",
    "`\n",
    "\n",
    "->\n",
    "\n",
    "`\n",
    "from neural_wrappers.pytorch import NeuralNetworkPyTorch\n",
    "class MyAwesomeNN(NeuralNetworkPyTorch):\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "        ...\n",
    "`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Model summary]\n",
      "General neural network architecture. Update __str__ in your model for more details when using summary.\n",
      "Parameters count: 0. Trainable parameters: 0.\n",
      "Hyperparameters:\n",
      "Metrics:\n",
      "\t- Loss (min)\n",
      "Callbacks: None\n",
      "Optimizer: None\n",
      "Optimizer Scheduler: None\n",
      "GPU: False\n"
     ]
    }
   ],
   "source": [
    "from neural_wrappers.pytorch import NeuralNetworkPyTorch\n",
    "class MyAwesomeNN(NeuralNetworkPyTorch):\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "        \n",
    "model = MyAwesomeNN()\n",
    "print(model.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### 2. Data\n",
    "\n",
    "The built-in class that handles datasets is `DatasetReader`, however it is not necessary to use it, since any regular data generator will suffice. When creating a new reader, one must subclass this class and implement the dataset specific data loading parts. We'll take as example a simple logistic regression task with 3 classes, generated from a Gaussian distribution with various means/stds.\n",
    "\n",
    "The important methods are `generator = iterate_once(Type, miniBatchSize, ...)`, which returns a generator that can be directly fed to `train_generator(generator, numSteps, numEpochs, ...)`. The other important method is `numSteps = getNumIterations(Type, miniBatchSize)`, which returns the number of iterations per epoch.\n",
    "\n",
    "Type is simply a string, that is usually only one of 3 possible values (95% of times 2 values): `train`, `validation`, `test`.\n",
    "\n",
    "The naming convention is x (data), t (targets) and y (outputs). Thus a loss function would look like `y = Model(x); L = loss(y, t).`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.1 Simple Dataset example\n",
    "\n",
    "We generate 3 Gaussian shaped vectors split as follows: 9999 data points for \"train\", 3333 data points for \"validation\" and 999 data points for \"test\". The first Gaussian represents class 1, the second one class 2, the third one class 3. Each data point has 5 features (all of them representing same Gaussian distribution). This is only to add more dimensions to the data.\n",
    "\n",
    "Thus, the data is divided as such:\n",
    "- *train* : (9999, 5) (divided in 3 * 3333 data points for each class)\n",
    "- *validation* : (3333, 5) (divided in 3 * 1111 data points for each class)\n",
    "- *test* : (999, 5) (divided in 3 * 333 data points for each class)\n",
    "\n",
    "The purpose is to train a small model that learns the clasisfication of the data points on the *train* set, validates the result on the *validation* set and, upon selecting the best model, test it on the unseen *test* set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "from neural_wrappers.readers import DatasetReader\n",
    "from neural_wrappers.utilities import toCategorical\n",
    "import numpy as np\n",
    "\n",
    "class Reader(DatasetReader):\n",
    "    def __init__(self):\n",
    "        self.numClasses = 3\n",
    "        # This field is expected s.t. the default getNumIterations(...) method works. One can overwrite it however,\n",
    "        #  if a more special formula for computing the number of iterations per epoch is required.\n",
    "        self.numData = {\"train\" : 9999, \"validation\" : 3333, \"test\" : 999}\n",
    "        means = [-1, 0, 1.33]\n",
    "        stds = [0, 0.5, 0]\n",
    "        self.numFeatures = 5\n",
    "        \n",
    "        self.data = {k : self.generateData(k, means, stds) for k in self.numData.keys()}\n",
    "        print(\"Dataset:\\n\\t-\", \" \\n\\t- \".join([\"%s : X: %s, t: %s\" % (k, self.data[k][0].shape, self.data[k][1].shape) \\\n",
    "                                      for k in self.data.keys()]) )\n",
    "              \n",
    "    def generateData(self, k, means, stds):\n",
    "        N = self.numData[k]\n",
    "        assert N % self.numClasses == 0\n",
    "        x = np.zeros((N, self.numFeatures), dtype=np.float32)\n",
    "        t = np.zeros((N, ), dtype=np.uint32)\n",
    "        for i in range(self.numClasses):\n",
    "            startIndex, endIndex = i * (N // 3), (i + 1) * (N // 3)\n",
    "            x[startIndex : endIndex] = np.random.randn(N // self.numClasses, self.numFeatures) * stds[i] + means[i]\n",
    "            t[startIndex : endIndex] = i\n",
    "        \n",
    "        # Shuffle the data so it's independent and identically distributed\n",
    "        np.random.seed(42)\n",
    "        perm = np.random.permutation(N)\n",
    "        x = x[perm]\n",
    "        # We also need to make the targets one hot encoded: [1] => [0, 1, 0] for 3 classes\n",
    "        t = t[perm]\n",
    "        t = toCategorical(t, numClasses=self.numClasses)\n",
    "        return x, t\n",
    "    \n",
    "    # This method is our generator, which must, at each step, return a batch of (X, t) items.\n",
    "    def iterate_once(self, type, miniBatchSize):\n",
    "        assert type in (\"train\", \"validation\", \"test\")\n",
    "\n",
    "        data = self.data[type]\n",
    "        x, t = data[0], data[1]\n",
    "        numIterations = self.getNumIterations(type, miniBatchSize)\n",
    "        for i in range(numIterations):\n",
    "            startIndex = i * miniBatchSize\n",
    "            endIndex = min((i + 1) * miniBatchSize, self.numData[type])\n",
    "            assert startIndex < endIndex, \"startIndex < endIndex. Got values: %d %d\" % (startIndex, endIndex)\n",
    "            yield x[startIndex : endIndex], t[startIndex : endIndex]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we've seen how a dataset generator should look like, let's play a little with the provided methods to see how they can help us."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2 Checking that the generator gives us relevant data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Dataset:\n",
      "\t- train : X: (9999, 5), t: (9999, 3) \n",
      "\t- validation : X: (3333, 5), t: (3333, 3) \n",
      "\t- test : X: (999, 5), t: (999, 3)\n",
      "Calling the generator and getting one batch\n",
      "First batch: (20, 5) [1 2 0 2 1 2 0 2 1 0 0 1 1 1 1 1 0 1 2 1]\n"
     ]
    }
   ],
   "source": [
    "reader = Reader()\n",
    "print(\"Calling the generator and getting one batch\")\n",
    "generator = reader.iterate_once(\"train\", miniBatchSize=20)\n",
    "x, t = next(generator)\n",
    "print(\"First batch:\", x.shape, t.argmax(axis=-1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.3 Checking the getNumIterations method with varying mini batch sizes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Varying batch size:\n",
      "\t-1 => 9999 iterations\n",
      "\t-2 => 5000 iterations\n",
      "\t-3 => 3333 iterations\n",
      "\t-5 => 2000 iterations\n",
      "\t-13 => 770 iterations\n",
      "\t-17 => 589 iterations\n",
      "\t-20 => 500 iterations\n"
     ]
    }
   ],
   "source": [
    "print(\"Varying batch size:\")\n",
    "for MB in [1, 2, 3, 5, 13, 17, 20]:\n",
    "    trainNumSteps = reader.getNumIterations(\"train\", miniBatchSize=MB)\n",
    "    print(\"\\t-%d => %d iterations\" % (MB, trainNumSteps))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### 3. Defining a model\n",
    "\n",
    "First, we define a model that takes the data (X), which has 5 features, and projects them into the 3 dimensional output, corresponding to the 3 classes. Then, we call the softmax function to get probabilities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch as tr\n",
    "import torch.nn as nn\n",
    "\n",
    "class Model(NeuralNetworkPyTorch):\n",
    "    def __init__(self, numFeatures, outClasses):\n",
    "        super().__init__()\n",
    "        self.fc1 = nn.Linear(in_features=numFeatures, out_features=outClasses)\n",
    "    \n",
    "    def forward(self, x):\n",
    "        y = self.fc1(x)\n",
    "        return y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can test the model by generating some random data. The library provides a helpful wrapper on top of the forward function, called npForward, which take numpy arrays, converts them to torch tensors, calls the forward function and converts the result back to numpy arrays.\n",
    "\n",
    "Note that the returing torch arrays are detached from the original computational graph, so one cannot train a model like this. However `train_generator` takes care of this. `npForward` is useful for testing purposes mostly, after the model has been trained."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Torch result shape: torch.Size([37, 3])\n",
      "Numpy result shape: (37, 3)\n"
     ]
    }
   ],
   "source": [
    "model = Model(numFeatures=5, outClasses=3)\n",
    "\n",
    "MB = 37\n",
    "trX = tr.randn(MB, 5)\n",
    "trY = model.forward(trX)\n",
    "print(\"Torch result shape:\", trY.shape)\n",
    "\n",
    "npX = np.random.randn(MB, 5).astype(np.float32)\n",
    "npY = model.npForward(npX)\n",
    "print(\"Numpy result shape:\", npY.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### 4. Training a Model\n",
    "\n",
    "The loss function is the negative log likelihood of the predicted probabilities against the expected one-hot encoded classes. This is standard multi class logistic regression.\n",
    "\n",
    "The class `NeuralNetworkPyTorch` expects to have a `criterion` set (loss function) and an `optimizer`, before being able to train the model on some data. These are set by `model.setCriterion(lossFn(y, t))` and `model.setOptimizer(optimizerType, **optimizerArgs)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([1, 3]) torch.Size([1, 3])\n",
      "tensor([[0.1000, 0.5000, 0.4000]])\n",
      "tensor([[False,  True, False]])\n",
      "tensor(0.6931)\n"
     ]
    }
   ],
   "source": [
    "import torch.nn.functional as F\n",
    "\n",
    "# Negative log-likeklihood (used for softmax+NLL for classification), expecting targets are one-hot encoded\n",
    "def lossFn(y, t, debug=False):\n",
    "    # Outputs are transformed into probabilities using the softmax class\n",
    "    y = F.softmax(y, dim=1)\n",
    "\n",
    "    # Targets are transformed into one-hot encoding using the toCatergorical helpful function, then turned into booleans\n",
    "    #  for boolean indexing.\n",
    "    t = t.type(tr.bool)\n",
    "\n",
    "    if debug:\n",
    "        print(y)\n",
    "        print(t)\n",
    "\n",
    "    # This is simply boolean accessing the output probability softmax(y) of the correct target's index. So if we have\n",
    "    #  y = [0.1, 0.5, 0.4] and the target is [0, 1, 0], then we'll have y[t] == 0.5.\n",
    "    # Since our expected value of 1 (log(1) == 0), we have a loss of -log(0.5) = 0.693, which is minimized using the optimizer.\n",
    "    return (-tr.log(y[t] + 1e-5)).mean()\n",
    "\n",
    "y = tr.FloatTensor(np.log([[0.1, 0.5, 0.4]]))\n",
    "t = tr.from_numpy(toCategorical([[1]], numClasses=3))\n",
    "print(y.shape, t.shape)\n",
    "L = lossFn(y, t, debug=True)\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's train the model. One more trick we can use is calling the `getGenerators` function that is simply a wrapper on top of `iterate_once` and `getNumIterations` discussed earlier for the `DatasetReader` class. \n",
    "\n",
    "We'll also use some standard callbacks, such as plotting the loss and saving the model, which are added using the `addCallbacks` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch 1/10. Done: 10.00%. LR: 0.01000. Took: 0:00:01.306542.  -Metrics. [Train] Accuracy: 86.459. Loss: 0.398. | [Validation] Accuracy: 97.725. Loss: 0.242. \n",
      "[SaveModels] Epoch 1. Improvement (Loss) from 0.24 to 0.24\n",
      "Epoch 2/10. Done: 20.00%. LR: 0.01000. Took: 0:00:01.954416.  -Metrics. [Train] Accuracy: 98.689. Loss: 0.186. | [Validation] Accuracy: 99.251. Loss: 0.148. \n",
      "[SaveModels] Epoch 2. Improvement (Loss) from 0.15 to 0.15\n",
      "Epoch 3/10. Done: 30.00%. LR: 0.01000. Took: 0:00:01.556539.  -Metrics. [Train] Accuracy: 99.370. Loss: 0.124. | [Validation] Accuracy: 99.461. Loss: 0.107. \n",
      "[SaveModels] Epoch 3. Improvement (Loss) from 0.11 to 0.11\n",
      "Epoch 4/10. Done: 40.00%. LR: 0.01000. Took: 0:00:01.713367.  -Metrics. [Train] Accuracy: 99.540. Loss: 0.093. | [Validation] Accuracy: 99.581. Loss: 0.085. \n",
      "[SaveModels] Epoch 4. Improvement (Loss) from 0.08 to 0.08\n",
      "Epoch 5/10. Done: 50.00%. LR: 0.01000. Took: 0:00:01.676225.  -Metrics. [Train] Accuracy: 99.610. Loss: 0.076. | [Validation] Accuracy: 99.611. Loss: 0.071. \n",
      "[SaveModels] Epoch 5. Improvement (Loss) from 0.07 to 0.07\n",
      "Epoch 6/10. Done: 60.00%. LR: 0.01000. Took: 0:00:01.648320.  -Metrics. [Train] Accuracy: 99.630. Loss: 0.064. | [Validation] Accuracy: 99.611. Loss: 0.061. \n",
      "[SaveModels] Epoch 6. Improvement (Loss) from 0.06 to 0.06\n",
      "Epoch 7/10. Done: 70.00%. LR: 0.01000. Took: 0:00:01.396733.  -Metrics. [Train] Accuracy: 99.660. Loss: 0.056. | [Validation] Accuracy: 99.701. Loss: 0.054. \n",
      "[SaveModels] Epoch 7. Improvement (Loss) from 0.05 to 0.05\n",
      "Epoch 8/10. Done: 80.00%. LR: 0.01000. Took: 0:00:01.517318.  -Metrics. [Train] Accuracy: 99.710. Loss: 0.050. | [Validation] Accuracy: 99.731. Loss: 0.048. \n",
      "[SaveModels] Epoch 8. Improvement (Loss) from 0.05 to 0.05\n",
      "Epoch 9/10. Done: 90.00%. LR: 0.01000. Took: 0:00:01.554826.  -Metrics. [Train] Accuracy: 99.720. Loss: 0.045. | [Validation] Accuracy: 99.731. Loss: 0.044. \n",
      "[SaveModels] Epoch 9. Improvement (Loss) from 0.04 to 0.04\n",
      "Epoch 10/10. Done: 100.00%. LR: 0.01000. Took: 0:00:01.503439.  -Metrics. [Train] Accuracy: 99.730. Loss: 0.041. | [Validation] Accuracy: 99.731. Loss: 0.041. \n",
      "[SaveModels] Epoch 10. Improvement (Loss) from 0.04 to 0.04\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYUAAAEICAYAAACwDehOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8QZhcZAAAgAElEQVR4nO3deXxU1d348c93ZrISEiAEQRACAlW2QIwLiCjFBZeK1Fag2FqxLq241Wptq099fH5t1cenFtvfj6daUGsRcKnQ1laLAlLLJmsSFgUxwIQtBJkQss7M9/fHTIYEAoQsczPJ993XvO6dM/ee+81Q73fuPfecI6qKMcYYA+ByOgBjjDGthyUFY4wxEZYUjDHGRFhSMMYYE2FJwRhjTIQlBWOMMRGWFIwxMUNEAiKyodbrsWasO1NE8huwXW8RWSIi60UkV0Sua64YWgOJ5X4KXbt21czMTKfDMMZEyfr16xkxYkSL1F1ZWcn27dsZPHjwKbfbuXMnycnJZGRkUF5ezvbt2xk6dGiLxNRS1q5de1BVM+r7zBPtYJpTZmYma9ascToMY0yUpKSk1PvffGZmJpMmTWLJkiUAvP766/Tv35+dO3cybdo0ioqKyMjI4OWXX6Z3797s37+fe+65hx07dgAwc+ZMzj77bK699lpGjBjB8uXL6dmzJwsXLiQpKanOse6++2769evHj3/8Y1asWMHDDz/M8uXLW/6Pb0YisvNkn9ntI2NMzCgvL2f48OGR1/z58yOfpaamsnr1aqZPn86DDz4IwPTp0/nOd75Dbm4uU6dO5f777wfg/vvv5/LLL2fjxo2sW7cucnWwbds27r33XjZt2kSnTp14++23T4jhySef5E9/+hO9evXiuuuu47e//W0U/vLoienbRzk5OWpXCsa0HykpKZSWlp5QnpmZyeLFi+nXrx/V1dV0796d4uJiunbtyt69e4mLi6O6upoePXpw8OBBMjIy8Hq9JCQkROooKCjgqquuYtu2bQA888wzVFdX8/jjj9c51q9//WtUlYcffpgVK1Zwxx13kJ+fj8sVO7+xRWStqubU91ns/BXGGHMKIlLv+sm2qU/tJOF2u/H7/SdsM2vWLG655RYARo4cSUVFBQcPHmxMyK2SJQVjTJtQcytp/vz5jBw5EoBRo0Yxb948AObMmcPo0aMBGDduHDNnzgQgEAhQUlLS4OP07t2bDz/8EIAtW7ZQUVFBRka9bbYxKaYbmo0x7UtNm0KN8ePH8/TTTwOhp4cuvvhigsEgc+fOBeCFF15g2rRp/Pd//3ekoRlgxowZ3HXXXcyaNQu3283MmTPp0aNHg2L4n//5H+68806ef/55RIRXXnnltFcgscTaFIwxMa/mScSuXbs6HUpMsDYFY4wxDWK3j4wxMa+goMDpENoMu1IwxhgT0WJJQURmi8iB2mOJiEgXEVkkItvCy861PvuJiGwXkU9F5JqWissYE7tEhG9/+9uR936/n4yMDG644YYm111VVcXtt9/O0KFDycrKYunSpZHP5s+fz7Bhwxg8eDCPPvpovfvPmTOnTsc6l8vFhg0bgFCDeFZWFoMHD+aee+4hEAg0Od6W0pJXCq8A448rewz4UFUHAB+G3yMig4DJwODwPv9PRNwtGJsxJgZ16NCB/Px8ysvLAVi0aBE9e/ZslrpfeuklAPLy8li0aBEPP/wwwWCQ4uJiHnnkET788EM2bdrE/v37I4+k1jZ16lQ2bNjAhg0beO2118jMzIw8KfXGG2+wceNG8vPzKSoq4s0332yWmFtCiyUFVV0GHDqueALwanj9VeCmWuXzVLVSVb8AtgMXtVRsxpjYde211/Luu+8CMHfuXKZMmRL5bPXq1YwaNYoRI0YwatQoPv30UyDUC3natGlA6KQ/ZMgQysrK6tS7efNmxo0bB0C3bt3o1KkTa9asYceOHQwcODDSF+HKK6+sd/iL2o6PKzU1FQhd2VRVVbXqR1ij3aZwlqruBQgvu4XLewK7a23nDZcZY0wdkydPZt68eVRUVJCbm8vFF18c+ey8885j2bJlrF+/nqeeeoqf/vSnADz44INs376dd955h9tvv53f//73JCcn16k3KyuLhQsX4vf7+eKLL1i7di27d++mf//+bN26lYKCAvx+PwsWLGD37t2cyvz58+skBYBrrrmGbt260bFjR77xjW8007fR/FrL00f1pc16O1CIyF3AXRDqWWiMadve3fEuM9bNYN/RfVT4K9idspuCggLmzp3LddfVncrA5/Nx2223sW3bNkSE6upqAFwuF6+88grDhg3j7rvv5tJLLz3hONOmTWPLli3k5OTQp08fRo0ahcfjoXPnzsycOZNJkybhcrkYNWpUZHTV+qxatYrk5GSGDBlSp/z999+noqKCqVOnsnjxYq666qpm+HaaX7SvFPaLSA+A8PJAuNwLnFNru17AnvoqUNUXVTVHVXPaUtdyY8yJ3t3xLk8uf5K9R/ei4f89ufxJzrvsPH70ox+d8Gv8iSeeYOzYseTn5/PXv/6VioqKyGfbtm0jJSWFPXvqPbXg8Xh4/vnn2bBhAwsXLuTw4cMMGDAAgK997WusWrWKFStW8JWvfCVSXp958+adEFeNxMREbrzxRhYuXHimX0XURPtK4S/AbcDT4eXCWuWvi8ivgbOBAcDqKMdmjGkOwSAE/aABCAZqLYPHvQ8v6ysLbztj9bNUBCrqVF8RqGBX/2088f3JDEosYql3PXr0IFWffcCXhds5K7sPlZ9+wB9++ypaXUH51kUcLjnKfd+/j/dm/4qHf/E7/vT8E0y4+rLQ7QhVFDhaVkFQoUNyIktWrIPKI5zl30Xxxl0cOHSYrp3T+LLkCC/8zzO89Mxj7F//d4Lh+xkaqoZgMMC811/jrd8/w+5P3kVRjpaVU3q0nIyunamuDvDma3/gwqxB7Fj519B+tWIIDTChkTqD4QOoHrt1UrNJh/Qe9B187NZZc2mxYS5EZC5wBdAV2A/8HFgAvAH0BnYB31TVQ+HtfwZMA/zAg6r6j9Mdw4a5MO2ZqhJUCASCBKvLCVSVE6wuR6vKCFaVEawuh6qKSJn6K6C6/NjLXw7VFYi/AvGHyiRQgcsfKnOF112BSlA/BIOIBiIvNIBoTVkQlwZx0byPWg7LPAet1Si7+e7NDPr9IESV3ILQff2lBX6eW17F376VzIrdfm5bUEFGB+GrmW5ey62m4MGOTFtYzvDuLu6/OIHdviBjXz3K8js60K3DsZslBYeDXPOnMlwCPTsKs25Mok+n0OdT3i5j474gAP9xeQKTh8QB8JdPq1mzJ8BTYxMjsTz2QSUrv9chUu/+0iA3zC2j0g8Bha9munl+fCIeV9Mam9d2HMsFDy9o1L6nGubCxj4yzgn4oaoUqo6GX0fAX9ngX5ca9OMP+An4/fj9fgKBmmWAQMBPwF9NIOAnGPATDAQIBkKfaTAQKdOgHw34CUbqDL/Cv9yg5tcboZsXeuyXWp33HPuVd+xzTqinzq/CWtvV/aV4rCxOq0igkgSqSNDwkioSa72SpKrR/wTlGk8FoVe5xlNZa72mvJI4ArgJqIsAQhAXftyEUkDoFQy/cLkBF+pyh9bFjbhcIB7U5UbC5RL+zOUOrYvLE166Ebcbl8uDy+3mJX2ZEk4cwTRN0rgv5QEEjr3CJ1kRECTSUBl5HykIby81jZlybF0k8hnhOo69r72v1KrjWN01TxXVF8Ox8lrHqbUtteqo+1nNcaXO8Tt06kafrxwbHPBMnCoptJaGZtPaqdY6eZceO5lX1lqvKa887kRfdZRgZSnBiiNoeB+X/yjuQGWTQhIgLvxqiIBK5AQWiLzckTLFRVBcBHHXevRBaq/WXtSJo97ta0qOK5DaH8jx+9epCr/E43cl4Hcn4nel4HclUO5O4IgrkaA7kUDklUDQnUjQk0jQnRRaehJRdxLqSYC4JNSTiHqSIC4RPEngScTtcuF2CS6X4HEJLhHcLiHBBUnhdZcIcW4Xce7Q0uMWPK7Qe4/bhccVKnc38ZdvfTJ29ODJ5U/WuYWU6E7kJ6N+wvX9rm/24xlLCu1LdTkcPQhlB0PLo0XHluVf1v3VXnmk1ok+XFb/A2EnHoY4KiSRMhI5SiJHggmUBBMpowNHSeeohsrLwsujJBKI64AroQMuTyJutweXxxNaut143HG4PR7cHg8etwd3nAePx0OcOy609MThqSnzxBEX5yHO4yEuLp64OA/xcfEkxHmIj3MT73aREOci3u0i3uMi2RNab83PjbdnNSf+mqePunfozgPZD1hCaEGWFGJZoLrWSb7mBF+zXgRlxcfWjxaHfrXXI+hOoDIujUpXMhWSSLkkcVQ7cETTKQkmUKLxHCKeQ/44jmoipZpEGQmUknTsxK6J+D3JxCWl0CE5mU5J8aQmxdEpOY60pDg6hddTk+LonRwfKUtLCpW1xK9M0zZc3+96SwJRZEmhNQkGoOxQrZN8+GQeOckf9wu/4nD99bg8kNwVOmRAh3TonBlaT06HDhkcdqWxvjiOj/coi3YG2XXURc1NC5cQOpknxZHWoe7Ju1NyHBnh9dD7+Eh5WlIciXE2Mokxsc6SglNUYfMCWPMylO4P/+Ivpv5bNBI5odOhK3QfGj7Jdw29rymvWSZ2qnMju6I6wCcFh/jXtoMsW1fE1n2hK4auKQmMHtCVBwdmkN27M507xNMxwYPLfrUb025ZUnDCoR3w90dg+weQ3h+6nQ+9R4ZP6jUn+K7H3id1Dj/V0TCqyrb9R1j2WRHLth1k1Y5iKv1B4t0ucjI78+Px5zFmYFfO755qCcAYU4clhWjyV8LyF2DZc+CKg/HPwIXfA3fT/xkOHa3i4+0HWfZZEf/aVsT+ktCTPf27pfCti3szZkAGF/frQnK8/ZMbY07OzhDR8sUy+NsPoXgbDLoJxv8KUs9udHVV/iDrdn3Jv7YV8a9tB8kr9KEKaUlxjB7QlTEDujJ6QAY9OyU14x9hjGnrLCm0tNIi+OfjkDsPOvWBqW/BgDMfCEtVKSgui1wJrPi8mKNVAdwuIbt3Jx66ciBjBmYwtGeaPcljjGk0SwotJRiEda/CBz+HqjIY8whc9jDENfyXu6+8mhWfH+Sjzw7yr21FeL8MTSzSJz2Zidk9uWxABiPPTSc1saHdt4wx5tQsKbSEfXnwt4fA+wlkXgbX/xoyBp52N38gyEavj39tK2LZZ0Vs2H2YoEJKgodR56Zz9+XnMmZAV/qkdzhtXcYY0xiWFJpT5RFY+jSsnBl6Ymji72HYpBPHOajF+2UZy8JXAv/efpCSCj8iMKxXJ6aP7c9lAzMYfk4n4tzRHuXcGNMeWVJoDqqw9W/wjx9DSSFc8F0Y93NI7nLK3RZt3s+dfwwN6NcjLZFrh/RgzMAMLu2fTqfk+CgEbowxdVlSaKovd8I/HoXP3oOzhsA3X4FzGja99L+3HyQ53s1fpl/KuRkpNv6OMcZxlhQay18FK34HHz0L4oKrfwEX33NGfQ5yvYcZcnYa/bt1bMFAjTGm4SwpNEbBv+HdH0LRVjj/azD+aUjrdUZV+ANBNu0p4dZL+rRQkMYYc+YsKZyJo8Ww6D9gw58grTdMmQ9fGd+oqrYdKKXSH2Roz7RmDtIYYxrPkkJDBIOhRLDoP0JPGI1+KNTvIL7xj4bmFfoAGNrLkoIxpvVwJCmIyAPAnYTGa35JVX8jIsOB/wUSCc3T/ANVXe1EfHXs3xzqc7B7JfQeBTf8OjSAXRPleX2kJHjoa30OjDGtSNSTgogMIZQQLgKqgPdE5F3gWeA/VfUfInJd+P0V0Y4vouoofPQMrPi/kJAKE/4fDP/WKfscnIncQh9DetoopcaY1sWJK4XzgZWqWgYgIh8BEwlNJJAa3iYN2ONAbCFb/x56zNS3G0Z8G6566rR9Ds5ElT/Ilr0lfHdUZrPVaYwxzcGJpJAP/EJE0oFy4DpgDfAg8L6IPAe4gFFRj+zw7lAHtE/fhW6DYNr70PuSZj/MZ/uPUGWNzMaYVijqSUFVt4jIM8AioBTYSKgN4fvAQ6r6tojcAswCrjx+fxG5C7gLoHfv3s0TVKA6NDTF0l+F3l/1FFzyA3C3zEBzNY3Mw6yR2RjTyjgyoI6qzlLVbFUdAxwCtgG3AX8Ob/ImoTaH+vZ9UVVzVDUnIyOj6cHsWgW/vxwWPQF9L4d7V8GlD7RYQoBQUkhN9NC7S3KLHcMYYxrDkaQgIt3Cy97A14G5hNoQLg9v8lVCiaLllB2Cv9wHs6+GCh9Mfh2+NQ86NdPVxynkeX0M7ZVmw1oYY1odp/opvB1uU6gG7lXVL0XkTmCGiHiACsK3iFqEdw28fguUH4ZR98PlP4aElBY7XG2V/gBb95Vwx+h+UTmeMcacCUeSgqpeVk/Zx8AFUQmg60DoPRKu+Al0HxKVQ9b4dN8RqgNq7QnGmFapffZoTkyFyXMcOXSuN9yT2Z48Msa0QjZzS5TleX10To6jV+eGT8tpjDHRYkkhyvIKfQzpaY3MxpjWyZJCFFVUB/hs/xFrTzDGtFqWFKJoy94S/EFlaM9OTodijDH1sqQQRdaT2RjT2llSiKJcr4+uKfH0SEt0OhRjjKmXJYUoyvP6GGqNzMaYVsySQpSUVfnZduAIQ3tZe4IxpvWypBAlW/aWEFTrtGaMad0sKURJTU9ma2Q2xrRmlhSiJM/ro1vHBM5KtUZmY0zrZUkhSnILfXaVYIxp9SwpREFppZ/Pi0qt05oxptWzpBAFmwp9qFp7gjGm9bOkEAU1PZmH2JNHxphWzpJCFOQV+uiRlkhGxwSnQzHGmFOypBAFNT2ZjTGmtbOk0MJKKqrZcfCotScYY2KCI0lBRB4QkXwR2SQiD9Yqv09EPg2XP+tEbM0tP9yeYMNbGGNiQdTnaBaRIcCdwEVAFfCeiLwL9AImAMNUtVJEukU7tpaQZ3MyG2NiSNSTAnA+sFJVywBE5CNgIpADPK2qlQCqesCB2JpdXqGPXp2T6NIh3ulQjDHmtJy4fZQPjBGRdBFJBq4DzgEGApeJyCoR+UhELqxvZxG5S0TWiMiaoqKiKIbdOHmF1shsjIkdUU8KqroFeAZYBLwHbAT8hK5aOgOXAI8Ab0g9Ew+o6ouqmqOqORkZGdELvBF8ZdXsLC5jqDUyG2NihCMNzao6S1WzVXUMcAjYBniBP2vIaiAIdHUivuYSmX7ThrcwxsQIJ9oUEJFuqnpARHoDXwdGEkoCXwWWishAIB446ER8zSW38DBgjczGmNjhSFIA3haRdKAauFdVvxSR2cBsEckn9FTSbaqqDsXXLPK8PvqkJ5OWHOd0KMYY0yCOJAVVvayesirgVgfCaTG5Xh8jetutI2NM7LAezS3k0NEqCg+X260jY0xMsaTQQvIiPZktKRhjYoclhRaS5w01Mttw2caYWGJJoYXken3069qB1ERrZDbGxA5LCi0kr9Bnt46MMTHHkkILKDpSyV5fhTUyG2NijiWFFlAzXPYwGy7bGBNjLCm0gFyvDxEYfHaq06EYY8wZsaTQAvIKD3NuRgodEpzqMG6MMY1jSaEF5Hp9DLP2BGNMDLKk0Mz2l1Rw4EilPXlkjIlJlhSaWa63ppHZkoIxJvZYUmhmeYU+XAKDelhSMMbEntMmBRGZLiKdoxFMW5DnPcyAbh1Jinc7HYoxxpyxhlwpdAc+EZE3RGR8fVNkmhBVtZ7MxpiYdtqkoKqPAwOAWcB3gW0i8ksRObeFY4s5e30VHCytsvYEY0zMalCbQngGtH3hlx/oDLwlIs+2YGwxp6aR2Ya3MMbEqtP2rhKR+4HbCM2X/AfgEVWtFhEXsA14tGVDjB15hYfxuITze1hPZmNMbGrIlUJX4Ouqeo2qvqmq1QCqGgRuaMxBReQBEckXkU0i8uBxn/1IRFREujambiflen0MPKsjiXHWyGyMiU0NSQp/Bw7VvBGRjiJyMYCqbjnTA4rIEOBO4CIgC7hBRAaEPzsHuArYdab1Ok1VyS/0WXuCMSamNSQpzARKa70/Gi5rrPOBlapapqp+4CNgYviz5wndjtIm1O8I75flfFlWbTOtGWNiWkOSgoQbmoHIbaOmjPSWD4wRkXQRSQauA84RkRuBQlXd2IS6HZNXaD2ZjTGxryEn9x3hxuaaq4MfADsae0BV3SIizwCLCF2BbCT0RNPPgKtPt7+I3AXcBdC7d+/GhtHscr0+4tzCV7p3dDoUY4xptIZcKdwDjAIKAS9wMeGTcmOp6ixVzVbVMYTaKwqAvsBGESkAegHrRKR7Pfu+qKo5qpqTkZHRlDCaVV7hYc7rnkqCxxqZjTGx67RXCqp6AJjcnAcVkW6qekBEegNfB0aq6oxanxcAOap6sDmP21JUlTyvjxuyznY6FGOMaZKG9FNIBO4ABgOJNeWqOq0Jx31bRNKBauBeVf2yCXU5btehMkoq/DaHgjEm5jWkTeE1YCtwDfAUMBU440dRa1PVy07zeWZT6o+2mp7M9uSRMSbWNaRNob+qPgEcVdVXgeuBoS0bVmzJK/QR73Ex8CxrZDbGxLaGJIXq8PJwuONZGpDZYhHFoFzvYc7vkUq8x6anMMbEtoacxV4Mz6fwOPAXYDPwTItGFUOCQSW/sMTaE4wxbcIp2xTCg96VhBuClwH9ohJVDPmi+CillX6bQ8EY0yac8koh3Ht5epRiiUl5NiezMaYNacjto0XhkUvPEZEuNa8WjyxG5BX6SIxz0T8jxelQjDGmyRrySGpNf4R7a5UpdisJCF0pDOqRisdtjczGmNjXkB7NfaMRSCwKBJX8PT5uyTnH6VCMMaZZNKRH83fqK1fVPzZ/OLFlR1EpZVUBm37TGNNmNOT20YW11hOBccA6oN0nhVxrZDbGtDENuX10X+33IpJGaOiLdi+v0EdyvJt+1shsjGkjGtM6WgYMaO5AYlFeoY8hZ6fhdonToRhjTLNoSJvCXzk2PaYLGAS80ZJBxQJ/IMimPT6+dVEfp0Mxxphm05A2hedqrfuBnarqbaF4Ysb2olIqqoPWnmCMaVMakhR2AXtVtQJARJJEJFNVC1o0slauppHZhrcwxrQlDWlTeBMI1nofCJe1a3leHykJHvqmd3A6FGOMaTYNSQoeVa2qeRNej2+5kGJDbqGPIT1TcVkjszGmDWlIUigSkRtr3ojIBCAm5k5uKdWBIFv2ljCsVyenQzHGmGbVkKRwD/BTEdklIruAHwN3N+WgIvKAiOSLyCYReTBc9t8islVEckXkHRFptWfcz/YfocoftJ7Mxpg257RJQVU/V9VLCD2KOlhVR6nq9sYeMDx7253ARUAWcIOIDAAWAUNUdRjwGfCTxh6jpdUMl21JwRjT1pw2KYjIL0Wkk6qWquoREeksIv+nCcc8H1ipqmWq6gc+Aiaq6j/D7wFWAr2acIwWlVvoo2Oihz7pyU6HYowxzaoht4+uVdXDNW/Cs7Bd14Rj5gNjRCRdRJLDdR0/zOg04B9NOEaLyvP6GNYrDRFrZDbGtC0NSQpuEUmoeSMiSUDCKbY/JVXdQmiO50XAe8BGQp3iaur/Wfj9nPr2F5G7RGSNiKwpKipqbBiNVukPsHVfCUN7ttomD2OMabSGJIU/AR+KyB0icgehk/mrTTmoqs5S1WxVHQMcArYBiMhtwA3AVFXVk+z7oqrmqGpORkZGU8JolE/3HaE6oNaT2RjTJjVklNRnRSQXuBIQQr/umzTgj4h0U9UDItIb+DowUkTGE3qy6XJVLWtK/S0pr9AamY0xbVdDhrkA2EeoV/MtwBfA20087tsikg5UA/eq6pci8jtCt6UWhe/Vr1TVe5p4nGaX5/XRKTmOXp2TnA7FGGOa3UmTgogMBCYDU4BiYD4gqjq2qQdV1cvqKevf1HqjIdfrY2hPa2Q2xrRNp2pT2EpolrWvqepoVf0toXGP2q2K6gCf7T9i7QnGmDbrVEnhZkK3jZaIyEsiMo5Qm0K7tWVvCf6g2pNHxpg266RJQVXfUdVJwHnAUuAh4CwRmSkiV0cpvlalppHZrhSMMW1VQ4a5OKqqc1T1BkK9jDcAj7V4ZK1QntdH15R4eqQlOh2KMca0iDOao1lVD6nq71X1qy0VUGuWV2iNzMaYtu2MkkJ7Vl4VamS2/gnGmLbMkkIDbd7rI6gw1OZQMMa0YZYUGqhmTmZrZDbGtGWWFBooz+ujW8cEzkq1RmZjTNtlSaGB8gp9dpVgjGnzLCk0wNFKP9uLSq3TmjGmzbOk0ACb9pSgCkN7pTodijHGtChLCg2Q6w1NPDfEHkc1xrRxlhQaIK/QR4+0RLp1tEZmY0zbZkmhAfLCw2UbY0xbZ0nhNEoqqtlx8Kg9eWSMaRcsKZzGpsISwHoyG2PaB0sKp5FXGGpktttHxpj2wJLCaeR6ffTslESXDvFOh2KMMS3OkaQgIg+ISL6IbBKRB8NlXURkkYhsCy87OxHb8awnszGmPYl6UhCRIcCdwEVAFnCDiAwgNHHPh6o6APiQVjCRj6+smp3FZQy1pGCMaSecuFI4H1ipqmWq6gc+AiYCE4BXw9u8CtzkQGx1RKbftOEtjDHthBNJIR8YIyLpIpIMXAecA5ylqnsBwstu9e0sIneJyBoRWVNUVNSigdYkBWtkNsa0F1FPCqq6BXgGWAS8B2wE/Gew/4uqmqOqORkZGS0UZUhe4WH6pCeTlhzXoscxxpjWwpGGZlWdparZqjoGOARsA/aLSA+A8PKAE7HVluv12XhHxph2xamnj7qFl72BrwNzgb8At4U3uQ1Y6ERsNQ4drcL7ZTnDLCkYY9oRj0PHfVtE0oFq4F5V/VJEngbeEJE7gF3ANx2KDajVnmBPHhlj2hFHkoKqXlZPWTEwzoFw6pVnw2UbY9oh69F8EnmFPvp17UBqojUyG2PaD0sKJ5Hn9dmtI2NMu2NJoR5FRyrZ46uw/gnGmHbHkkI98q3TmjGmnbKkUI9crw8RGGxJwRjTzlhSqEde4WHOzUghJcGpJ3aNMcYZlhTqkev1Wac1Y0y7ZEnhOPtLKjhwpNKePDLGtEuWFI6T5w0Pl21JwRjTDllSOE5uoQ+XwKAelhSMMe2PJYXj5HkPM6BbR5Li3U6HYowxUdGuv5QAABI2SURBVGdJoRZVJa/QejIbY9ovSwq17PVVcLC0ytoTjDHtliWFWmz6TWNMe2dJoZY8rw+PSzi/R6rToRhjjCMsKdSSW+hj4FkdSYyzRmZjTPtkSSFMVcnzHrZbR8aYds2SQpj3y3K+LKu2J4+MMe2aI0lBRB4SkU0iki8ic0UkUUSGi8hKEdkgImtE5KJoxlTTyGxPHhlj2rOoJwUR6QncD+So6hDADUwGngX+U1WHA/8Rfh81uV4fcW7hK907RvOwxhjTqjh1+8gDJImIB0gG9gAK1Dz2kxYui5r8Qh/ndU8lwWONzMaY9ivqEwaoaqGIPAfsAsqBf6rqP0VkN/B++DMXMCqKMZHrPcwNWWdH65DGGNMqOXH7qDMwAegLnA10EJFbge8DD6nqOcBDwKyT7H9XuM1hTVFRUbPEtOtQGSUVfnvyyBjT7jkxtdiVwBeqWgQgIn8mdFUwFXggvM2bwB/q21lVXwReBMjJydHmCCjXaz2ZjTmd6upqvF4vFRUVTodiGigxMZFevXoRFxfX4H2cSAq7gEtEJJnQ7aNxwBpCbQiXA0uBrwLbohVQXqGPeI+LgWdZI7MxJ+P1eunYsSOZmZmIiNPhmNNQVYqLi/F6vfTt27fB+znRprBKRN4C1gF+YD2hX/7rgRnhxucK4K5oxZTrPcz5PVKJ91i3DWNOpqKiwhJCDBER0tPTOdPb7I7MTK+qPwd+flzxx8AF0Y4lGFQ2FZZw04ie0T60MTHHEkJsacy/V7v/aVxQfJQjlX7ryWxMK1dcXMzw4cMZPnw43bt3p2fPnpH3VVVVDarj9ttv59NPPz3jY19//fVcdtllZ7xfLHLkSqE1seGyjYkN6enpbNiwAYAnn3ySlJQUfvSjH9XZRlVRVVyu+n/vvvzyy2d83OLiYvLy8khMTGTXrl307t37zINvAL/fj8fj/Cm53V8p5Hp9JHhcDOiW4nQoxphG2L59O0OGDOGee+4hOzubvXv3ctddd5GTk8PgwYN56qmnItuOHj2aDRs24Pf76dSpE4899hhZWVmMHDmSAwcO1Fv/W2+9xU033cSkSZOYP39+pHzfvn1MmDCBYcOGkZWVxapVq4BQ4qkpu/322wG49dZbWbBgQWTflJTQ+eaDDz7gyiuvZPLkyYwYMQKAr33ta1xwwQUMHjyYP/zh2EOY7777LtnZ2WRlZXH11VcTCATo378/hw4dAiAQCNCvX7/I+8ZyPi05LM/rY/DZqXjc7T4/GtNg//nXTWzeU9KsdQ46O5Wff21wo/bdvHkzL7/8Mv/7v/8LwNNPP02XLl3w+/2MHTuWb3zjGwwaNKjOPj6fj8svv5ynn36aH/7wh8yePZvHHnvshLrnzp3Lr371K9LS0rj11lt55JFHALj33nu56qqrmD59On6/n7KyMjZu3MgzzzzD8uXL6dKlS4NO0CtXrmTz5s2RK5BXX32VLl26UFZWRk5ODjfffDOVlZV8//vf51//+hd9+vTh0KFDuN1upkyZwuuvv8706dN5//33ufDCC+nSpUujvsMa7fpMGAgq+Xt8DOvVyelQjDFNcO6553LhhRdG3s+dO5fs7Gyys7PZsmULmzdvPmGfpKQkrr32WgAuuOACCgoKTtimsLCQXbt2cckllzBo0CACgQBbt24FYOnSpdx9990AeDweUlNTWbx4MZMmTYqcmBtygh45cmSdW1LPP/985OrF6/Xy+eefs2LFCsaOHUufPn3q1HvHHXfw6quvAjB79uzIlUlTtOsrhS8OllJWFbD2BGPOUGN/0beUDh06RNa3bdvGjBkzWL16NZ06deLWW2+tt8NdfHx8ZN3tduP3+0/YZv78+RQXF0ee8/f5fMybN48nn3wSOPHpHlWt94kfj8dDMBgEQrd5ah+rduwffPABy5YtY+XKlSQlJTF69GgqKipOWm9mZiadO3dmyZIlrF+/nquvvrre7+dMtOsrhZqezDZctjFtR0lJCR07diQ1NZW9e/fy/vvvN7quuXPn8sEHH1BQUEBBQQGrV69m7ty5AIwdOzZyuyoQCFBSUsKVV17JvHnzIreNapaZmZmsXbsWgHfeeYdAIFDv8Xw+H126dCEpKYlNmzbxySefAHDppZeyePFidu7cWadeCF0tTJ06lcmTJ5+0gf1MtPukkBzvpl+GNTIb01ZkZ2czaNAghgwZwp133smll17aqHo+//xz9u3bR05OTqRswIABJCQksHbtWn73u9/x/vvvM3ToUHJycti6dSvDhg3j0UcfZcyYMQwfPjzS/nD33XezaNEiLrroIjZs2EBCQkK9x7z++uspKysjKyuLp556iosvvhiAs846i5kzZzJhwgSysrKYOnVqZJ+JEyfi8/n47ne/26i/83ii2izDBzkiJydH16xZ0+j9b565HJfAm/dEbUBWY2LWli1bOP/8850Owxxn5cqV/OQnP2HJkiX1fl7fv5uIrFXVnPq2b7dXCv5AkE17fAztaY3MxpjY9Itf/IJJkybxy1/+stnqbLdJYXtRKRXVQWtPMMbErJ/97Gfs3LmTkSNHNlud7TYpRIbLtqRgjDER7TYp5Bf6SEnw0De9w+k3NsaYdqLdJoVcr48hPVNxuWzUR2OMqdEuk0J1IMjmvSXWac0YY47TLpPCZ/uPUOUPMtSGtzAmZlxxxRUndET7zW9+ww9+8INT7lcz+Fx93nnnHUQkMnSFaadJISMlgf+4YRAXZTZt4ChjTPRMmTKFefPm1SmbN28eU6ZMaXSdc+fOZfTo0SfU29xO1oO5NWqXSaFbaiLTRvele1qi06EYYxroG9/4Bn/729+orKwEoKCggD179jB69GhKS0sZN24c2dnZDB06lIULF562vtLSUv79738za9asE5LCs88+y9ChQ8nKyoqMnLp9+3auvPJKsrKyyM7O5vPPP2fp0qXccMMNkf2mT5/OK6+8AoSGtnjqqacYPXo0b775Ji+99BIXXnghWVlZ3HzzzZSVlQGwf/9+Jk6cSFZWFllZWSxfvpwnnniCGTNmROr92c9+xgsvvNCk76+hHBkQT0QeAr4HKJAH3K6qFSJyHzCd0NzN76rqo07EZ4w5jX88BvvymrfO7kPh2qdP+nF6ejoXXXQR7733HhMmTGDevHlMmjQJESExMZF33nmH1NRUDh48yCWXXMKNN954yukoFyxYwPjx4xk4cCBdunRh3bp1ZGdn849//IMFCxawatUqkpOTI+MMTZ06lccee4yJEydSUVFBMBhk9+7dp/yTEhMT+fjjj4HQZD133nknAI8//jizZs3ivvvu4/777+fyyy+PjIlUWlrK2Wefzde//nUeeOABgsEg8+bNY/Xq1Wf6jTZK1JOCiPQE7gcGqWq5iLwBTBaRncAEYJiqVopIt2jHZoxp3WpuIdUkhdmzZwOh0Ul/+tOfsmzZMlwuF4WFhezfv5/u3buftK65c+fy4IMPAjB58uTIcNsffPABt99+O8nJyUBomOojR45QWFjIxIkTgdDJviEmTZoUWc/Pz+fxxx/n8OHDlJaWcs011wCwePFi/vjHPwKh0VrT0tJIS0sjPT2d9evXs3//fkaMGEF6evoZfluN49TQ2R4gSUSqgWRgD/B94GlVrQRQ1fqnQTLGOO8Uv+hb0k033cQPf/hD1q1bR3l5OdnZ2QDMmTOHoqIi1q5dS1xcHJmZmfUOl12juLiYxYsXk5+fj4gQCAQQEZ599tl6h6k+2RhxtYfEBk44Zu1hsb/73e+yYMECsrKyeOWVV1i6dOkp/9bvfe97vPLKK+zbt49p06adctvmFPU2BVUtBJ4DdgF7AZ+q/hMYCFwmIqtE5CMRufBU9Rhj2p+UlBSuuOIKpk2bVqeB2efz0a1bN+Li4liyZElkiOmTeeutt/jOd77Dzp07KSgoYPfu3fTt25ePP/6Yq6++mtmzZ0fu+R86dIjU1FR69eoVmVKzsrKSsrIy+vTpw+bNm6msrMTn8/Hhhx+e9JhHjhyhR48eVFdXM2fOnEj5uHHjmDlzJnBsCG4IjX763nvv8cknn0SuKqIh6klBRDoTuk3UFzgb6CAitxK6eugMXAI8Arwh9dwQFJG7RGSNiKwpKiqKYuTGmNZgypQpbNy4kcmTJ0fKpk6dypo1a8jJyWHOnDmcd955p6xj7ty5kVtBNW6++WZef/11xo8fz4033khOTg7Dhw/nueeeA+C1117jhRdeYNiwYYwaNYp9+/ZxzjnncMsttzBs2DCmTp0amWe5Pv/1X//FxRdfzFVXXVUnvhkzZrBkyRKGDh3KBRdcwKZNm4DQJEBjx47llltuwe12n/H31FhRHzpbRL4JjFfVO8Lvv0MoEfQjdPtoabj8c+ASVT3pmb+pQ2cbYxrOhs6OrmAwSHZ2Nm+++SYDBgxodD2xMHT2LuASEUkOXwmMA7YAC4CvAojIQCAeOOhAfMYY46jNmzfTv39/xo0b16SE0BhRb2hW1VUi8hawjtCjp+uBFwk9njpbRPKBKuA2jeUZgIwxppEGDRrEjh07HDm2I08fqerPgZ/X89Gt0Y7FGGPMMe2yR7MxpnHs4j22NObfy5KCMaZBEhMTKS4utsQQI1SV4uLiBne0q+FU5zVjTIzp1asXXq8XexQ8diQmJtKrV68z2seSgjGmQeLi4ujbt6/TYZgWZrePjDHGRFhSMMYYE2FJwRhjTETUh7loTiJSBJx65KvWryvWc7s2+z7qsu/jGPsu6mrK99FHVTPq+yCmk0JbICJrTjYGSXtk30dd9n0cY99FXS31fdjtI2OMMRGWFIwxxkRYUnDei04H0MrY91GXfR/H2HdRV4t8H9amYIwxJsKuFIwxxkRYUnCIiJwjIktEZIuIbBKRB5yOyWki4haR9SLyN6djcZqIdBKRt0Rka/j/IyOdjslJIvJQ+L+TfBGZKyJnNspbjBOR2SJyIDzfTE1ZFxFZJCLbwsvOzXEsSwrO8QMPq+r5hKYjvVdEBjkck9MeIDQLn4EZwHuqeh6QRTv+XkSkJ3A/kKOqQwA3MPnUe7U5rwDjjyt7DPhQVQcAH4bfN5klBYeo6l5VXRdeP0LoP/qezkblHBHpBVwP/MHpWJwmIqnAGGAWgKpWqephZ6NynAdIEhEPkAzscTieqFLVZcCh44onAK+G118FbmqOY1lSaAVEJBMYAaxyNhJH/QZ4FAg6HUgr0A8oAl4O3077g4h0cDoop6hqIfAcofnd9wI+Vf2ns1G1Cmep6l4I/cgEujVHpZYUHCYiKcDbwIOqWuJ0PE4QkRuAA6q61ulYWgkPkA3MVNURwFGa6dZALArfK58A9AXOBjqIiE3d20IsKThIROIIJYQ5qvpnp+Nx0KXAjSJSAMwDvioif3I2JEd5Aa+q1lw5vkUoSbRXVwJfqGqRqlYDfwZGORxTa7BfRHoAhJcHmqNSSwoOEREhdM94i6r+2ul4nKSqP1HVXqqaSagBcbGqtttfgqq6D9gtIl8JF40DNjsYktN2AZeISHL4v5txtOOG91r+AtwWXr8NWNgcldrMa865FPg2kCciG8JlP1XVvzsYk2k97gPmiEg8sAO43eF4HKOqq0TkLWAdoaf21tPOejeLyFzgCqCriHiBnwNPA2+IyB2EEuc3m+VY1qPZGGNMDbt9ZIwxJsKSgjHGmAhLCsYYYyIsKRhjjImwpGCMMSbCkoIxpyEiARHZUOvVbL2LRSSz9siXxjjN+ikYc3rlqjrc6SCMiQa7UjCmkUSkQESeEZHV4Vf/cHkfEflQRHLDy97h8rNE5B0R2Rh+1QzV4BaRl8LzBfxTRJIc+6NMu2dJwZjTSzru9tGkWp+VqOpFwO8IjfRKeP2PqjoMmAO8EC5/AfhIVbMIjWW0KVw+APi/qjoYOAzc3MJ/jzEnZT2ajTkNESlV1ZR6yguAr6rqjvDghvtUNV1EDgI9VLU6XL5XVbuKSBHQS1Ura9WRCSwKT5SCiPwYiFPV/9Pyf5kxJ7IrBWOaRk+yfrJt6lNZaz2AtfUZB1lSMKZpJtVargivL+fYdJFTgY/D6x8C34fIfNSp0QrSmIayXyTGnF5SrZFsITR3cs1jqQkisorQD6wp4bL7gdki8gihGdRqRjh9AHgxPKplgFCC2Nvi0RtzBqxNwZhGCrcp5KjqQadjMaa52O0jY4wxEXalYIwxJsKuFIwxxkRYUjDGGBNhScEYY0yEJQVjjDERlhSMMcZEWFIwxhgT8f8BKWTFk0oxvdkAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "import torch.optim as optim\n",
    "from neural_wrappers.utilities import getGenerators\n",
    "from neural_wrappers.callbacks import SaveModels, PlotMetrics\n",
    "from neural_wrappers.metrics import Accuracy\n",
    "\n",
    "trainGenerator, trainSteps, valGenerator, valSteps = getGenerators(reader, miniBatchSize=20, keys=[\"train\", \"validation\"])\n",
    "\n",
    "model = Model(numFeatures=5, outClasses=3)\n",
    "model.setCriterion(lossFn)\n",
    "model.setOptimizer(optim.SGD, lr=0.01, momentum=0.5)\n",
    "model.addCallbacks([SaveModels(\"best\"), PlotMetrics([\"Loss\", \"Accuracy\"])])\n",
    "model.addMetrics({\"Accuracy\" : Accuracy()})\n",
    "\n",
    "# Train for 10 epochs using the defined data generators.\n",
    "model.train_generator(trainGenerator, trainSteps, 10, valGenerator, valSteps, printMessage=\"v1\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### 5. Loading the stored model and testing\n",
    "By default models using the `SaveModels()` callback are stored under the name `model_best_<metric>.pkl`, in this case `model_best_Loss.pkl`. We'll load it using a new instance of the model and compute the loss on the test set. To load a model, we can use `loadWeights` or `loadModel`. The later one also loads the history of the model as well as optimizer hyperparameters and other info. It can be useful to continue a training loop that was stopped too soon by calling again `train_generator` with the loaded model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Loading model from model_best_Loss.pkl\n",
      "Succesfully loaded weights (18 parameters) \n",
      "Finished loading model\n",
      "{'Loss': 0.04091725905713208, 'Accuracy': 99.79998004000399}\n"
     ]
    }
   ],
   "source": [
    "model = Model(numFeatures=5, outClasses=3)\n",
    "model.setCriterion(lossFn)\n",
    "model.addMetrics({\"Accuracy\" : Accuracy()})\n",
    "model.loadWeights(\"model_best_Loss.pkl\")\n",
    "\n",
    "testGenerator, testNumSteps = reader.iterate_once(\"test\", miniBatchSize=20), reader.getNumIterations(\"test\", miniBatchSize=20)\n",
    "metrics = model.test_generator(testGenerator, testNumSteps)\n",
    "print(metrics)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
